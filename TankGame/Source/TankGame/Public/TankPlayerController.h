// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */

class UTankAimingComponent;
class UPhysicsHandleComponent;

UCLASS()
class TANKGAME_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void FoundAimingComponent(UTankAimingComponent* AimCompRef);

private:

	void AimTowardCrosshair();
	bool GetSightRayHitLocation(FVector&) const;

	bool GetLookDirection(FVector2D ScreenLocation, FVector &WorldDirection) const;
	bool GetLookVectorHitLocation(FVector, FVector&) const ;

	void FindPhysicsHandleComponent();
	const FHitResult GetFirstPhysicsBodyInReach();


private:
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	
	UPROPERTY(EditAnywhere)
		float CrosshairLocationX = 0.5f;
	UPROPERTY(EditAnywhere)
		float CrosshairLocationY = 0.3333f;
	UPROPERTY(EditAnywhere)
		float LineTraceRange = 1000000; // 10Km

};
