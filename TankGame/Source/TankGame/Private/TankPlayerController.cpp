// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"

#include "TankAimingComponent.h"

#include "GameFramework/Controller.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/PrimitiveComponent.h" 
#include "Camera/PlayerCameraManager.h"


// Called at launch
void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	

	//auto ControlledTank = getControlledTank();
	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();

	if (!ensure(AimingComponent)) return;

	FoundAimingComponent(AimingComponent);
	
}

// Called every frame
void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardCrosshair();
}


void ATankPlayerController::AimTowardCrosshair()
{
	if (!GetPawn()) return;
		auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if ( !ensure(AimingComponent) ) return;

	FVector HitLocation;
	bool bGotHitLocation = GetSightRayHitLocation(HitLocation);
	if (bGotHitLocation)
	{
		AimingComponent->AimAt(HitLocation);
	}
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
	int32 ViewportSizeX, ViewportSizeY;
	this->GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector2D ScreenLocation = FVector2D(ViewportSizeX * CrosshairLocationX, ViewportSizeY * CrosshairLocationY);

	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Look Direction : %s"), *LookDirection.ToString());
		return GetLookVectorHitLocation(LookDirection, HitLocation);
	}

	return false;
}

bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& WorldDirection) const
{
	FVector CameraWorldLocation;
	return this->DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraWorldLocation, WorldDirection);
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const
{
	FHitResult HitResult;
	auto StartLocation = PlayerCameraManager->GetCameraLocation();
	auto EndLocation = StartLocation + (LookDirection * LineTraceRange);
	if (this->GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
	{
		//UE_LOG(LogTemp, Warning, TEXT("StartLocation %s EndLocation %s"), *StartLocation.ToString(), *EndLocation.ToString());
		HitLocation = HitResult.Location;
		return true;
	}
	return false;
}

void ATankPlayerController::FindPhysicsHandleComponent()
{
}

const FHitResult ATankPlayerController::GetFirstPhysicsBodyInReach()
{
	return FHitResult();
}


