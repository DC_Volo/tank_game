// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"

#include "TankTrack.h"


void UTankMovementComponent::Initialise(UTankTrack* r_track, UTankTrack* l_track)
{
	if (!r_track || !l_track)
	{
		return;
	}
	 LeftTrack = l_track ;
	 RightTrack = r_track ;
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	FVector TankForward = this->GetOwner()->GetActorForwardVector().GetSafeNormal();
	FVector ForwardIntention = MoveVelocity.GetSafeNormal();
	auto ForwardThrow = FVector::DotProduct(TankForward, ForwardIntention);
	IntendMoveForward(ForwardThrow);
	auto RightThrow = FVector::CrossProduct(TankForward, ForwardIntention).GetSafeNormal().Z;
	IntendMoveRight(RightThrow);
}

void UTankMovementComponent::IntendMoveForward(float Throw)
{

	if ( !ensure(LeftTrack) && !ensure(RightTrack) )
		return;
	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(Throw);
}

void UTankMovementComponent::IntendMoveRight(float Throw)
{
	if ( !ensure(LeftTrack || !RightTrack) )
		return;
	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(-Throw);
}