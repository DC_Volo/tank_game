// Fill out your copyright notice in the Description page of Project Settings.

#include "AITankController.h"

#include "TankAimingComponent.h"

#include "Engine/World.h"


void AAITankController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto ControlledTank = this->GetPawn();

	if (!ensure(PlayerTank && ControlledTank)) return;

	MoveToActor(PlayerTank, AcceptanceRadius);

	auto AimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	AimingComponent->AimAt(PlayerTank->GetActorLocation());

	if(AimingComponent->GetFiringStatus() == EFiringStatus::Locked)
		AimingComponent->Fire();
}

void AAITankController::BeginPlay()
{
	Super::BeginPlay();


}